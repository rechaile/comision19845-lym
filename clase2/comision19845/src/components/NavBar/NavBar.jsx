// import {  , Navbar,  } from 'react-bootstrap'

import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Titulo from '../Titulo/Titulo'
import { Link, NavLink } from 'react-router-dom'

function NavBar( ) {
  return (
    <>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          {/* <Titulo /> */}
          <Container >
          <NavLink to="/">React-Ecommerce</NavLink>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="me-auto">
              <NavLink  to="/categoria/gorras" className={({isActive})=> isActive ?'' :'' } >Gorras</NavLink>
              <NavLink to="/categoria/remeras">Remeras</NavLink>            
              </Nav>
              <Nav>
              {/* <Nav.Link href="#deets">More deets</Nav.Link> */}
              <NavLink to='/cart' >
                  icono carrito                 
              </NavLink>
              </Nav>
          </Navbar.Collapse>
          </Container>
      </Navbar>
      

    </>
  )
}

export default NavBar