import { useState } from 'react'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import Cart from '../components/Cart/Cart'
import NavBar from '../components/NavBar/NavBar'
import Titulo from '../components/Titulo/Titulo'
import ItemDetailContainer from '../container/ItemDetailContainer/ItemDetailContainer'
import ItemListContainer from '../container/ItemListContainer/ItemListContainer'
// import imagen from '../../src/logo.svg'

function RoutesApp() {
    const obj = { tit: 'Soy titulo de App', sutTit: 'soy sub tit' }  // estado

    return (
        <BrowserRouter>
            <NavBar  />          
            <Routes>
                <Route 
                    path="/" 
                    element={<ItemListContainer 
                        greeting='Hola soy ItemListContainer' 
                        titulo= { Titulo }
                    />} 
                />              
                <Route 
                    path="/categoria/:categoriaId" 
                    element={<ItemListContainer 
                        greeting='Hola soy ItemListContainer' 
                        titulo= { Titulo }
                    />} 
                />              
                <Route path="/detalle/:detalleId" element={<ItemDetailContainer />} />
                <Route path="/cart" element={<Cart />} />
                {/* <Route path="/notfound" element={<NotFound />} /> */}

                {/* <Route path="/*" element={<Navigate to='/notfound' replace/> } /> */}
                <Route path="/*" element={<Navigate to='/' replace/> } />
                
            </Routes>
        </BrowserRouter>
    )
}

export default RoutesApp