import {useState, useEffect} from 'react'
// import './input.css'


export const Input = () => {

    const inputHandler = (event)=>{
        //event.stopPropagation()
        event.preventDefault()
        console.log(event)       
        
    }

    
    return (
        <div className="box" >
            <div className="border border-1 border-warning" >
                <input 
                    className="m-5" 
                    onKeyDown={ inputHandler } 
                    // onClick={ inputHandler } 
                    type="text" 
                    name="i" 
                    id="i"
                />
            </div>
        </div>
    )
}
