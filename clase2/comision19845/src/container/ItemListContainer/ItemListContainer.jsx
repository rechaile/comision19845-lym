import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import ItemList from '../../components/ItemList/ItemList'
import { getFetch } from '../../helpers/getFetch'


function ItemListContainer({greeting, titulo}) {

    const [productos, setProductos] = useState([])
    const [loading, setLoading] = useState(true)

    // hook de react router dom
    const { categoriaId } = useParams()

    useEffect(()=> {
        if (categoriaId) {
            getFetch // funcion que simula una api
            // .then(resp =>{ 
            //     //throw new Error('ESto es un error')//insatanciando un error
            //     console.log(resp)
            //     return resp
            // })
            .then(resp => setProductos(resp.filter(item => item.categoria === categoriaId)) )
            .catch(err => console.log(err))
            .finally(() => setLoading(false))            
            
        } else {            
            getFetch // funcion que simula una api
            // .then(resp =>{ 
            //     //throw new Error('ESto es un error')//insatanciando un error
            //     console.log(resp)
            //     return resp
            // })
            .then(resp => setProductos(resp) )
            .catch(err => console.log(err))
            .finally(() => setLoading(false))            
        }
    }, [categoriaId])

    const getFetchApi = async () => {
        try {
            const query = await fetch('/assets/DATA.json')// por defecto va un verbo tipo get
            const queryParse = await query.json()
            console.log(queryParse)
        } catch (error) {
            console.log(error)
        }
        
    }


    //como usar la función fetch
    // useEffect(()=>{
    //     fetch('/assets/DATA.json')// por defecto va un verbo tipo get
    //     .then(data => data.json())
    //     .then(resp => console.log(resp))
    // }, [])

    useEffect( ()=>{
        getFetchApi()
    }, [])




    console.log(categoriaId)
    return (
        <div>
            {greeting}<hr />
            {/* [1,2,3] => [<li>1</li>,<li>2</li>...] */}
            {/* { [1,2,3,4].map((nro)=> <li key={nro} >{nro}</li> ) }           */}
            {   loading ? 
                    <h2>Cargando...</h2>
                : 
                    <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                        <ItemList productos={productos} />
                    </div>                   
            
            }          

        </div> 
    )
}

export default ItemListContainer