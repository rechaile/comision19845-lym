import {useEffect, useState} from 'react'
import ItemDetail from "../../components/ItemDetail/ItemDetail"
import { getFetch, getFetchOne } from '../../helpers/getFetch'

function ItemDetailContainer() {
  const [producto, setProducto] = useState( {} )

  // usar useParam => detalleId

  useEffect(()=> {
    getFetchOne // funcion que simula una api    
    .then(resp => setProducto(resp) )
    .catch(err => console.log(err))
    .finally(() => setLoading(false))
}, [])

  return (
    <>
      <ItemDetail producto={producto} />
    </>
  )
}

export default ItemDetailContainer